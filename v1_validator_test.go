package botconf_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	config "gitlab.com/botbin/botconf"
)

type V1ValidatorTestSuite struct {
	suite.Suite
}

// TestValidate tests the Validate method of v1Validator.
// The test cases here should all be valid YAML; we're just
// interested in version violations.
func (ys *V1ValidatorTestSuite) TestValidate() {
	tests := []validateTestCase{
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    tag: 1.2.3
    author: tester
    scopes:
    - message
    trigger: "ping"`,
			IsValid: true,
    },
    {
			Content: `version: "1"
metadata:
  name: test
  version: 0.1.0
spec:
  trigger: "."
  modules:
  - name: ping
    author: tester
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "2"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
spec:
  trigger: "."
  modules:
  - name: ping
    tag: 1.2.3
    author: tester
    scopes:
    - message
    trigger: "ping"`,
			IsValid: true,
		},
		{
			Content: `version: "1"
metadata:
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: 1
metadata:
  name: test
  version: 0.1.0
spec:
  trigger: "."
  modules:
  - name: ping
    tag: 1.2.3
    author: tester
    scopes:
    - message`,
			IsValid: true,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."`,
			IsValid: false,
    },
    {
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    tag: 1.2.3
    scopes:
    - message
    trigger: "ping"`,
			IsValid: false,
    },
	}

	ys.runValidateTestCases(tests)
}

func (ys *V1ValidatorTestSuite) runValidateTestCases(tests []validateTestCase) {
	for _, test := range tests {
		loader := config.NewYAMLLoader()
		conf, err := loader.Load([]byte(test.Content))
		assert.Nil(ys.T(), err)

		validator := config.NewV1Validator()

		if test.IsValid {
			assert.Nil(ys.T(), validator.Validate(conf))
		} else {
			assert.NotNil(ys.T(), validator.Validate(conf))
		}
	}
}

type validateTestCase struct {
	Content string
	IsValid bool
}

func TestV1Validator(t *testing.T) {
	suite.Run(t, new(V1ValidatorTestSuite))
}
