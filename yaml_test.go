package botconf_test

import (
	"testing"

	config "gitlab.com/botbin/botconf"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type YAMLConfigLoaderTestSuite struct {
	suite.Suite
}

// TestLoad tests the yaml formats given to the Load method.
// These are about, "did you give me valid yaml", and not
// about "did you actually follow the directions"
func (ys *YAMLConfigLoaderTestSuite) TestLoad() {
	tests := []loadTestCase{
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: true,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
      message
    trigger: "ping"`,
			IsValid: false,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: 0.1.0
spec:
  trigger: "."
  modules:
  - name: ping
    scopes:
    - message
    trigger: "ping"`,
			IsValid: true,
		},
		{
			Content: `version: "1"
metadata:
  name: test
  version: "0.1.0"
spec:
  trigger: "."
  modules:
    name: ping
      scopes:
      - message
      trigger: "ping"`,
			IsValid: false,
		},
	}
	ys.runLoadTestCases(tests)
}

func (ys *YAMLConfigLoaderTestSuite) runLoadTestCases(tests []loadTestCase) {
	for _, test := range tests {
		loader := config.NewYAMLLoader()
		config, err := loader.Load([]byte(test.Content))

		if test.IsValid {
			assert.Nil(ys.T(), err)
			assert.NotNil(ys.T(), config)
		} else {
			assert.NotNil(ys.T(), err)
		}
	}
}

type loadTestCase struct {
	Content string
	IsValid bool
}

func TestYAMLConfigLoader(t *testing.T) {
	suite.Run(t, new(YAMLConfigLoaderTestSuite))
}
