package botconf

// BotConfig defines configuration data for a bot.
type BotConfig struct {
	// Version is the version for the configuration.
	// Different versions may require different fields, so
	// we need an indicator of what we're working with.
	Version string

	Metadata BotMetadata
	Spec     BotSpec
}

// BotMetadata defines meta information about the bot itself.
type BotMetadata struct {
	// Name defines the name that a user has given the bot.
	Name string

	// Version defines the version that a user has given the bot.
	Version string
}

// BotSpec defines how the bot should behave.
type BotSpec struct {
	// Trigger is the text that will precede each command.
	// E.g., "."  => ".roll"
	Trigger string

	// Modules defines each module that the bot will use.
	Modules []Module
}

// Module defines a module that a bot uses to process events.
type Module struct {
	// Name is the name of the untagged module.
	// E.g., coin-flip
	Name string

	// Author is the username of the module creator.
	Author string

	// Tag specifies which version of the module is being used.
	// E.g., "1.0"
	Tag string

	// Scopes define what a module can do on behalf of a bot.
	Scopes []string

	// Trigger is the text that will precede commands send to the module.
	// E.g., "dex" => "dex blastoise"
	Trigger string
}
