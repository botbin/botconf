package botconf

import (
	"errors"
	"fmt"
)

// v1Validator is a Validator for version 1 configurations.
type v1Validator struct {
}

// NewV1Validator returns a Validator for version 1 configurations.
func NewV1Validator() Validator {
	return v1Validator{}
}

// Validate ensures the config conforms to the version 1 spec.
func (v1 v1Validator) Validate(conf *BotConfig) (errs []error) {
	if err := v1.checkVersion(conf); err != nil {
		errs = append(errs, err)
	}

	if err := v1.checkMetadata(&conf.Metadata); err != nil {
		errs = append(errs, err)
	}

	if errs2 := v1.checkSpec(&conf.Spec); len(errs2) != 0 {
		errs = append(errs, errs2...)
	}
	return
}

func (v1 v1Validator) checkVersion(conf *BotConfig) error {
	if conf.Version != "1" {
		return errors.New(`expected version "1"`)
	}
	return nil
}

func (v1 v1Validator) checkMetadata(meta *BotMetadata) error {
	if meta.Name == "" {
		return errors.New("expected value for metadata.name")
	}
	// version is optional.
	return nil
}

func (v1 v1Validator) checkSpec(spec *BotSpec) (errs []error) {
	if spec.Trigger == "" {
		errs = append(errs, errors.New("expected value for spec.trigger"))
	}

	if len(spec.Modules) == 0 {
		errs = append(errs, errors.New("there must be at least one module definition"))
		return
	}

	for i, module := range spec.Modules {
		errs = append(errs, v1.checkModule(i, &module)...)
	}
	return
}

func (v1 v1Validator) checkModule(i int, module *Module) (errs []error) {
	if module.Name == "" {
		errs = append(errs, fmt.Errorf("expected value for spec.modules[%d].name", i))
	}

	if module.Author == "" {
		errs = append(errs, fmt.Errorf("expected value for spec.modules[%d].author", i))
	}

	if module.Tag == "" {
		errs = append(errs, fmt.Errorf("expected value for spec.modules[%d].version", i))
	}

	if len(module.Scopes) == 0 {
		errs = append(errs, fmt.Errorf("expected at least one scope for spec.modules[%d]", i))
	}
	return
}
